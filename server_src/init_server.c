/*
** init_server.c for  in /home/tran_w/Projects/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Wed Apr 24 12:13:17 2013 tran_w
** Last update Wed Apr 24 17:21:40 2013 tran_w
*/

#include		<stdlib.h>
#include		<unistd.h>
#include		<stdio.h>
#include		<errno.h>
#include		"server.h"

static int		init_socket(t_server *server)
{
  if ((server->s_i.sock = socket(AF_INET,
				 SOCK_STREAM,
				 server->s_i.pe->p_proto)) == R_FAILURE)
    {
      perror(ERR_SOCKET);
      return (R_FAILURE);
    }
  server->s_i.s_in.sin_family = AF_INET;
  server->s_i.s_in.sin_port = htons(server->port);
  server->s_i.s_in.sin_addr.s_addr = htons(INADDR_ANY);
  return (R_SUCCESS);
}

static int		init_protocol(t_server *server)
{
  if ((server->s_i.pe = getprotobyname(PROTOCOL)) == NULL)
    {
      perror(ERR_PROTOCOL);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

int			init_server(t_server *server)
{
  server->channels = NULL;
  server->clients = NULL;
  if (init_protocol(server) == R_FAILURE)
    return (R_FAILURE);
  if (init_socket(server) == R_FAILURE)
    return (R_FAILURE);
  if (bind(server->s_i.sock,
	   (const struct sockaddr *)&server->s_i.s_in,
	   sizeof(server->s_i.s_in)))
    {
      perror(ERR_BIND);
      close(server->s_i.sock);
      return (R_FAILURE);
    }
  if (listen(server->s_i.sock, MAX_CLIENTS))
    {
      perror(ERR_LISTEN);
      close(server->s_i.sock);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}
