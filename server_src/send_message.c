/*
** send_message.c for  in /home/tran_w//Project/my_irc
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Thu Jun 27 18:13:41 2013 duy-laurent tran
** Last update Thu Jun 27 18:14:14 2013 duy-laurent tran
*/

#include		<stdlib.h>
#include		<string.h>
#include		"server.h"

int			get_channel(const t_server *server,
				    const t_client *sender)
{
  t_client		*client;
  t_channel		*current_channel;
  unsigned int	       	i;
  unsigned int	       	j;

  i = 0;
  while (i < list_get_size(server->channels))
    {
      j = 0;
      current_channel = list_get_elem_at_position(server->channels, i++);
      while (j < list_get_size(current_channel->clients))
	{
	  client = list_get_elem_at_position(current_channel->clients, j++);
	  if (client->sock == sender->sock)
	    return (i);
	}
    }
  return (R_FAILURE);
}

static int		send_message_to_all_in_channel(const t_channel *channel,
						       const char *message,
						       t_client *sender)
{
  t_client		*client;
  unsigned int		i;
  char			result[BUFFER_SIZE];

  i = 0;
  while (i < list_get_size(channel->clients))
    {
      client = list_get_elem_at_position(channel->clients, i);
      if (client->sock != sender->sock)
	{
	  strncpy(result, sender->name, strlen(sender->name));
	  strcat(result, " : ");
	  strncat(result, message, BUFFER_SIZE - strlen(result));
	  if (my_send(client->sock, result, strlen(result)) == R_FAILURE)
	    return (R_FAILURE);
	  bzero(result, BUFFER_SIZE);
	}
      ++i;
    }
  return (R_SUCCESS);
}

int			send_message(t_server *server,
				     t_client *sender,
				     const char *message)
{
  t_channel		*channel;
  int			pos;

  if (server->channels != NULL)
    {
      if ((pos = get_channel(server, sender)) != R_FAILURE)
	{
	  channel = list_get_elem_at_position(server->channels, pos - 1);
	  if (send_message_to_all_in_channel(channel,
					     message, sender) == R_FAILURE)
	    return (R_FAILURE);
	}
    }
  return (R_SUCCESS);
}
