/*
** loop.c for  in /home/tran_w/Projects/my_irc/server_src
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Wed Apr 24 15:10:00 2013 tran_w
** Last update Sun Apr 28 20:51:54 2013 richard naina
*/

#include	       	<stdlib.h>
#include		<unistd.h>
#include		<stdio.h>
#include		<errno.h>
#include		"simple_list.h"
#include		"server.h"

static int		set_max_fd(t_server *server)
{
  t_client		*client;

  client = NULL;
  if (list_get_size(server->clients) == 0)
    return (server->s_i.sock);
  client = list_get_elem_at_front(server->clients);
  return (client->sock);
}

static void		set_fds(t_server *server)
{

  unsigned int		i;
  unsigned int		nb_clients;
  t_client		*client;

  client = NULL;
  FD_ZERO(&server->rfds);
  FD_ZERO(&server->wfds);
  FD_SET(STDIN_FILENO, &server->rfds);
  FD_SET(server->s_i.sock, &server->rfds);
  if (list_is_empty(server->clients) == FALSE)
    {
      if (server->fd_max == -1)
	server->fd_max = set_max_fd(server);
      i = 0;
      nb_clients = list_get_size(server->clients);
      while (i < nb_clients)
	{
	  client = list_get_elem_at_position(server->clients, i++);
	  FD_SET(client->sock, &server->rfds);
	  FD_SET(client->sock, &server->wfds);
	  if (client->sock > server->fd_max)
	    server->fd_max = client->sock;
	}
    }
}

int			loop(t_server *server)
{
  while (IS_TRUE)
    {
      set_fds(server);
      if ((select(set_max_fd(server) + 1, &server->rfds, &server->wfds,
		  NULL, NULL)) == R_FAILURE)
	return (R_FAILURE);
      if (FD_ISSET(STDIN_FILENO, &server->rfds))
	return (R_SUCCESS);
      else if (FD_ISSET(server->s_i.sock, &server->rfds))
	add_client(server);
      else
	respond(server);
      usleep(1);
    }
}
