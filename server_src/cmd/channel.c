/*
** join_channel.c for join in /home/naina_r//my_irc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Sun Apr 28 12:57:11 2013 richard naina
** Last update Sun Apr 28 21:03:15 2013 tran_w
*/

#include		<stdio.h>
#include		<string.h>
#include		"server.h"

int			join_channel(t_server *server,
				     t_client *client,
				     const char *cmd)
{
  char			**tab;
  t_channel		*channel;
  int			ret;

  tab = my_str_to_wordtab(cmd, " ");
  if (tablen(tab) != 2)
    return (R_FAILURE);
  ret = create_channel(server, tab[1]);
  channel = list_get_elem_at_position(server->channels, ret);
  list_add_elem_at_front(&channel->clients, client);
  free_tab(tab);
  return (R_SUCCESS);
}

int			part_channel(t_server *server,
				     t_client *sender,
				     const char *name)
{
  char			**tab;
  unsigned int		i;
  unsigned int		j;
  t_client		*client;
  t_channel		*channel;

  i = 0;
  tab = my_str_to_wordtab(name, " ");
  while (i < list_get_size(server->channels) && tablen(tab) == 2)
    {
      j = 0;
      channel = list_get_elem_at_position(server->channels, i++);
      if (strcmp(channel->name, tab[1]) == 0)
	{
	  while (j < list_get_size(channel->clients))
	    {
	      client = list_get_elem_at_position(channel->clients, j++);
	      if (client->sock == sender->sock)
		list_del_elem_at_position(&channel->clients, --j);
	    }
	}
    }
  free_tab(tab);
  return (R_SUCCESS);
}

int			list_channels(t_server *server,
				      t_client *sender,
				      const char *name)
{
  char			**tab;
  unsigned int		i;
  t_channel		*channel;

  i = 0;
  tab = my_str_to_wordtab(name, " ");
  while (i < list_get_size(server->channels))
    {
      channel = list_get_elem_at_position(server->channels, i);
      if (tablen(tab) == 2)
	{
	  if (strstr(channel->name, tab[1]) != NULL)
	    my_send(sender->sock, channel->name, strlen(channel->name));
	}
      else
	my_send(sender->sock, channel->name, strlen(channel->name));
      ++i;
    }
  return (R_SUCCESS);
}
