/*
** users.c for users in /home/naina_r//my_irc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Sun Apr 28 16:28:10 2013 richard naina
** Last update Sun Apr 28 21:04:09 2013 tran_w
*/

#include		<string.h>
#include		"server.h"

int			nickname(t_server *server,
				 t_client *sender,
				 const char *cmd)
{
  t_channel		*current_channel;
  t_client		*client;
  char			**tab;
  unsigned int		i;
  unsigned int		j;

  i = 0;
  server = server;
  tab = my_str_to_wordtab(cmd, " ");
  if (tablen(tab) != 2)
    return (R_FAILURE);
  while (i < list_get_size(server->channels))
    {
      j = 0;
      current_channel = list_get_elem_at_position(server->channels, i++);
      while (j < list_get_size(current_channel->clients))
	{
	  client = list_get_elem_at_position(current_channel->clients, j++);
	  if (strcmp(client->name, tab[1]) == 0)
	    return (R_FAILURE);
	}
    }
  sender->name = strdup(tab[1]);
  free_tab(tab);
  return (R_FAILURE);
}

int			users(t_server *server, t_client *sender)
{
  t_client		*client;
  t_channel		*channel;
  int			pos;
  unsigned int		i;

  i = 0;
  if ((pos = get_channel(server, sender)) == R_FAILURE)
    return (R_FAILURE);
  channel = list_get_elem_at_position(server->channels, pos - 1);
  while (i < list_get_size(channel->clients))
    {
      client = list_get_elem_at_position(channel->clients, i++);
      if (my_send(sender->sock, client->name,
		  strlen(client->name)) == R_FAILURE)
	return (R_FAILURE);
    }
  return (R_SUCCESS);
}
