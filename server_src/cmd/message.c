/*
** message.c for message in /home/naina_r//my_irc
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Sun Apr 28 18:49:31 2013 richard naina
** Last update Sun Apr 28 22:02:12 2013 tran_w
*/

#include		<string.h>
#include		"server.h"

static int		send_private_message(t_client *client,
					     t_client *sender,
					     char **tab,
					     const char *cmd)
{
  char			message[BUFFER_SIZE];

  strncpy(message, sender->name, strlen(sender->name));
  strcat(message, " : ");
  strncat(message, cmd + strlen(tab[0]) + strlen(tab[1]) + 2,
	  BUFFER_SIZE - (strlen(cmd) - strlen(tab[0]) - strlen(tab[1])));
  my_send(client->sock, message, strlen(message));
  bzero(message, BUFFER_SIZE);
  return (R_SUCCESS);
}

int			message(t_server *server,
				t_client *sender,
				const char *cmd)
{
  char			**tab;
  t_client		*client;
  unsigned int		j;

  j = 0;
  tab = my_str_to_wordtab(cmd, " ");
  if (tablen(tab) < 3)
    return (R_FAILURE);
  while (j < list_get_size(server->clients))
    {
      client = list_get_elem_at_position(server->clients, j++);
      if (!strcmp(client->name, tab[1]))
	send_private_message(client, sender, tab, cmd);
    }
  free_tab(tab);
  return (R_SUCCESS);
}
