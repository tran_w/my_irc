/*
** create_channel.c for  in /home/tran_w/Projects/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sat Apr 27 17:55:00 2013 tran_w
** Last update Sun Apr 28 22:03:35 2013 tran_w
*/

#include		<stdlib.h>
#include		<string.h>
#include		"server.h"

static int		does_exist(t_list channel, const char *name)
{
  unsigned int		i;
  t_channel		*tmp;

  i = 0;
  while (i < list_get_size(channel))
    {
      tmp = list_get_elem_at_position(channel, i);
      if (strcmp(tmp->name, name) == 0)
	return (i);
      ++i;
    }
  return (R_FAILURE);
}

int			create_channel(t_server *server, const char *name)
{
  t_channel		*channel;

  if (does_exist(server->channels, name) != R_FAILURE)
    return (R_SUCCESS);
  if ((channel = malloc(sizeof(t_channel))) == NULL)
    return (R_FAILURE);
  if ((channel->name = strdup(name)) == NULL)
    return (R_FAILURE);
  channel->clients = NULL;
  list_add_elem_at_front(&server->channels, channel);
  return (R_SUCCESS);
}
