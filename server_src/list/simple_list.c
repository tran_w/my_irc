/*
** simple_list.c for  in /home/tran_w/Projects/my_irc/server_src/list
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Wed Apr 24 17:08:25 2013 tran_w
** Last update Sun Apr 28 22:07:48 2013 tran_w
*/

#include		<stdlib.h>
#include		"simple_list.h"

unsigned int		list_get_size(t_list list)
{
  unsigned int		i;

  i = 0;
  while (list != NULL)
    {
      i++;
      list = list->next;
    }
  return (i);
}

t_bool			list_is_empty(t_list list)
{
  if (list != NULL)
    return (FALSE);
  return (TRUE);
}
