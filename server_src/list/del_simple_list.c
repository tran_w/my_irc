/*
** del_simple_list.c for  in /tmp/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 28 22:05:58 2013 tran_w
** Last update Sun Apr 28 22:06:34 2013 tran_w
*/

#include	       	<stdlib.h>
#include       		"simple_list.h"

t_bool			list_del_elem_at_back(t_list *front_ptr)
{
  t_list		tmp;

  tmp = (*front_ptr);
  if (tmp == NULL)
    return (FALSE);
  if (tmp->next == NULL)
    {
      free(tmp);
      return (TRUE);
    }
  while (tmp->next->next != NULL)
    tmp = tmp->next;
  free(tmp->next);
  tmp->next = NULL;
  return (TRUE);
}

t_bool			list_del_elem_at_front(t_list *front_ptr)
{
  t_list		tmp;

  tmp = (*front_ptr);
  if (tmp == NULL)
    return (FALSE);
  free(tmp);
  *front_ptr = tmp->next;
  return (TRUE);
}

t_bool			list_del_elem_at_position(t_list *front_ptr,
						  unsigned int position)
{
  t_list		tmp;
  t_list		save;
  unsigned int		i;

  tmp = (*front_ptr);
  if (tmp == NULL)
    return (FALSE);
  else if (position == 0)
    {
      list_del_elem_at_front(front_ptr);
      return (TRUE);
    }
  else if (position == list_get_size(*front_ptr))
    {
      list_del_elem_at_back(front_ptr);
      return (TRUE);
    }
  for (i = 0; i < position - 1; ++i)
    tmp = tmp->next;
  save = tmp->next;
  tmp->next = tmp->next->next;
  free(save);
  return (TRUE);
}
