/*
** get_simple_list.c for  in /tmp/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 28 22:07:11 2013 tran_w
** Last update Sun Apr 28 22:07:40 2013 tran_w
*/

#include		<stdlib.h>
#include		"simple_list.h"

void			*list_get_elem_at_front(t_list list)
{
  if (list == NULL)
    return (0);
  return (list->elem);
}
void			*list_get_elem_at_back(t_list list)
{
  if (list == NULL)
    return (0);
  while (list->next != NULL)
    list = list->next;
  return (list->elem);
}

void			*list_get_elem_at_position(t_list list,
						  unsigned int position)
{
  unsigned int		i;
  unsigned int		position_max;

  i = 0;
  position_max = list_get_size(list);
  if (list == NULL || position >= position_max)
    return (0);
  else if (position == 0)
    list_get_elem_at_front(list);
  else if (position == position_max)
    list_get_elem_at_back(list);
  while (i < position)
    {
      list = list->next;
      i++;
    }
  return (list->elem);
}

t_node			*list_get_first_node_with_elem(t_list list, void *elem)
{
  t_list		tmp;

  tmp = list;
  if (list == NULL)
    return (NULL);
  while (tmp != NULL)
    {
      if (tmp->elem == elem)
	return (list);
      tmp = tmp->next;
    }
  return (NULL);
}
