/*
** add_simple_list.c for  in /tmp/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 28 22:04:45 2013 tran_w
** Last update Sun Apr 28 22:07:02 2013 tran_w
*/

#include		<stdlib.h>
#include		"simple_list.h"

t_bool			list_add_elem_at_front(t_list *front_ptr, void *elem)
{
  t_list		new_elem;

  if ((new_elem = malloc(sizeof(t_node))) == NULL)
    return (FALSE);
  new_elem->elem = elem;
  if ((*front_ptr) == NULL)
    {
      new_elem->next = NULL;
      (*front_ptr) = new_elem;
    }
  else
    {
      new_elem->next = (*front_ptr);
      (*front_ptr) = new_elem;
    }
  return (TRUE);
}

t_bool			list_add_elem_at_back(t_list *front_ptr, void *elem)
{
  t_list		new_elem;
  t_list		tmp;

  tmp = (*front_ptr);
  if ((new_elem = malloc(sizeof(t_node))) == NULL)
    return (FALSE);
  new_elem->elem = elem;
  new_elem->next = NULL;
  if (*front_ptr == NULL)
    (*front_ptr) = new_elem;
  else
    {
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_elem;
    }
  return (TRUE);
}

t_bool			list_add_elem_at_position(t_list * front_ptr,
						  void *elem,
						  unsigned int position)
{
  t_list		new_elem;
  t_list		tmp;
  unsigned int		i;

  tmp = (*front_ptr);
  if ((new_elem = malloc(sizeof(t_node))) == NULL)
    return (FALSE);
  new_elem->elem = elem;
  if (*front_ptr == NULL)
    {
      new_elem->next = NULL;
      (*front_ptr) = new_elem;
    }
  else if (position == 0)
    list_add_elem_at_front(front_ptr, elem);
  else if (position == list_get_size(*front_ptr))
    list_add_elem_at_back(front_ptr, elem);
  else
    {
      for (i = 0; i < position - 1; ++i)
	tmp = tmp->next;
      new_elem->next = tmp->next;
      tmp->next = new_elem;
    }
  return (TRUE);
}
