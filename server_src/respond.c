/*
** respond.c for  in /home/tran_w/Projects/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Thu Apr 25 15:18:25 2013 tran_w
** Last update Sun Apr 28 20:51:25 2013 richard naina
*/

#include		<stdlib.h>
#include		<errno.h>
#include		<stdio.h>
#include		<unistd.h>
#include		"server.h"

static int		disconnect_client(t_server *server,
					  const unsigned int pos)
{
  t_client		*client;

  client = list_get_elem_at_position(server->clients, pos);
  if (close(client->sock) == R_FAILURE)
    {
      perror(ERR_CLOSE);
      return (R_FAILURE);
    }
  list_del_elem_at_position(&server->clients, pos);
  return (R_SUCCESS);
}

int			respond(t_server *server)
{
  int			i;
  t_client		*client;
  char			buffer[BUFFER_SIZE];

  i = -1;
  if (list_is_empty(server->clients) == FALSE)
    {
      while (++i < (int)list_get_size(server->clients))
	{
	  client = list_get_elem_at_position(server->clients, i);
	  if (FD_ISSET(client->sock, &server->rfds) &&
	      FD_ISSET(client->sock, &server->wfds))
	    {
	      if (my_recv(client->sock, buffer, BUFFER_SIZE) <= 0)
		{
		  if (disconnect_client(server, i--) == R_FAILURE)
		    return (R_FAILURE);
		  respond(server);
		}
	      else if (execute_request(server, client, buffer) == R_FAILURE)
		return (R_FAILURE);
	    }
	}
    }
  return (R_SUCCESS);
}
