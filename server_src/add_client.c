/*
** add_client.c for  in /home/tran_w/Projects/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Thu Apr 25 12:05:58 2013 tran_w
** Last update Sun Apr 28 21:09:06 2013 tran_w
*/

#include		<stdlib.h>
#include		<unistd.h>
#include		<string.h>
#include		<errno.h>
#include		<stdio.h>
#include		"server.h"

static int		get_client_ip(t_client *client, struct sockaddr_in s_in)
{
  if ((client->ip = inet_ntoa(s_in.sin_addr)) == NULL)
    {
      perror(ERR_IP);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

static int		get_client_socket(t_client *client,
					  const t_server *server)
{
  socklen_t		s_insize;
  struct sockaddr_in	s_in;

  s_insize = sizeof(s_in);
  if ((client->sock = accept(server->s_i.sock,
			     (struct sockaddr *)&s_in,
			     &s_insize)) == R_FAILURE)
    {
      perror(ERR_ACCEPT);
      return (R_FAILURE);
    }
  if (get_client_ip(client, s_in) == R_FAILURE)
    return (R_FAILURE);
  return (R_SUCCESS);
}

int			add_client(t_server *server)
{
  t_client		*client;

  if ((client = malloc(sizeof(t_client))) == NULL)
    {
      perror(ERR_MALLOC);
      return (R_FAILURE);
    }
  if (get_client_socket(client, server) == R_FAILURE)
    return (R_FAILURE);
  if ((client->name = strdup(DEFAULT_NAME)) == NULL)
    return (R_FAILURE);
  list_add_elem_at_front(&server->clients, client);
  printf("%s %s %s\n", SERVER_NAME, client->ip, GET_CONNECTED);
  return (R_SUCCESS);
}
