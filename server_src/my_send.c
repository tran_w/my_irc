/*
** my_send.c for  in /home/tran_w/Projects/my_irc/server_src
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Thu Apr 25 13:27:56 2013 tran_w
** Last update Sat Apr 27 17:40:25 2013 tran_w
*/

#include	<unistd.h>
#include	<stdio.h>
#include	"server.h"

int		my_send(const int socket,
			const char *buffer,
			const size_t len)
{
  int		byte;

  if ((byte = write(socket, buffer, len)) == R_FAILURE)
    {
      perror(ERR_SEND);
      return (R_FAILURE);
    }
  return (byte);
}
