/*
** execute_request.c for  in /home/tran_w//Project/my_irc
** 
** Made by duy-laurent tran
** Login   <tran_w@epitech.net>
** 
** Started on  Fri Apr 26 16:34:44 2013 duy-laurent tran
** Last update Sun Apr 28 21:01:50 2013 tran_w
*/

#include		<string.h>
#include		<stdio.h>
#include		"server.h"

static int		debug()
{
  puts(NOT_IMPLEMENTED);
  return (R_SUCCESS);
}

static t_commands	g_cmd[NB_SERV_CMDS] =
{
  {"/nick", &nickname},
  {"/users", &users},
  {"/join", &join_channel},
  {"/part", &part_channel},
  {"/list", &list_channels},
  {"/msg", &message},
  {"/send_file", &debug},
  {"/accept_file", &debug}
};

static int		is_cmd(const char *cmd)
{
  unsigned int		i;

  i = 0;
  while (i < NB_SERV_CMDS)
    {
      if (!strncmp(g_cmd[i].name, cmd, strlen(g_cmd[i].name)))
	return (i);
      ++i;
    }
  return (R_FAILURE);
}

int			execute_request(t_server *server,
					t_client *client,
					const char *cmd)
{
  int			ret;

  if ((ret = is_cmd(cmd)) != R_FAILURE)
    g_cmd[ret].ptr_func(server, client, cmd);
  else
    send_message(server, client, cmd);
  return (R_SUCCESS);
}
