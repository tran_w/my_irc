/*
** main.c for  in /home/tran_w/Projects/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Wed Apr 24 12:00:35 2013 tran_w
** Last update Sat Apr 27 13:09:15 2013 tran_w
*/

#include	<stdlib.h>
#include	<errno.h>
#include	<unistd.h>
#include	<stdio.h>
#include	"server.h"

int		main(int ac, char **av)
{
  t_server	server;

  if (ac != 2)
    {
      usage();
      return (EXIT_FAILURE);
    }
  server.port = atoi(av[1]);
  server.fd_max = -1;
  if (init_server(&server) == R_FAILURE)
    return (EXIT_FAILURE);
  if (loop(&server) == R_FAILURE)
    {
      perror(ERR_LOOP);
      close(server.s_i.sock);
      return (EXIT_FAILURE);
    }
  /* to do freeforall*/
  close(server.s_i.sock);
  puts(GOODBYE);
  return (EXIT_SUCCESS);
}
