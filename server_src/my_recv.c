/*
** my_recv.c for  in /home/tran_w/Projects/my_irc/server_src
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Thu Apr 25 13:29:54 2013 tran_w
** Last update Mon Jun 17 22:35:15 2013 tran_w
*/

#include	<unistd.h>
#include	<stdio.h>
#include	"server.h"

int		my_recv(const int socket,
			char *buffer,
			const size_t len)
{
  int		byte;

  if ((byte = read(socket, buffer, len - 1)) <= R_FAILURE)
    {
      perror(ERR_RECV);
      byte = 0;
    }
  buffer[byte] = '\0';
  return (byte);
}
