/*
** usage.c for  in /home/tran_w/Projects/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Wed Apr 24 15:15:35 2013 tran_w
** Last update Wed Apr 24 15:15:36 2013 tran_w
*/

#include	<stdio.h>
#include	"server.h"

void		usage(void)
{
  printf("How to use my_server:\n");
  printf("%s\n", SERVER_USAGE);
}
