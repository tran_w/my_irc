/*
** usage.c for  in /tmp/my_irc/client_src
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Sun Apr 28 19:34:19 2013 tran_w
** Last update Sun Apr 28 22:10:59 2013 tran_w
*/

#include	<stdio.h>
#include	"client.h"

int		usage()
{
  puts("\nUsage of my_irc:");
  puts("/help : to show usage.");
  puts("/server _host_[ :_port_] to connect to server.");
  puts("/nick _nickname_ : defines the bickname of the user in the channel.");
  puts("/list [string] : list the channels available on the server.\
Displays only the channels containing the string 'string' if it is specified.");
  puts("/join _channel_ : joins a channel on server.");
  puts("/part _channel_ : leave the channel.");
  puts("/users : display the users connected to the channel \
(display the nicknames of course).");
  puts("/msg nickname _message_ : sends a message to a specific user.");
  puts("_message_ : sends a message to all users connected to the channel.");
  puts("\n");
  return (R_SUCCESS);
}
