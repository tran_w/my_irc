/*
** do_connect.c for  in /home/tran_w/Projects/my_irc/client_src
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr 22 18:55:16 2013 tran_w
** Last update Mon Apr 22 19:01:20 2013 tran_w
*/

#include		<stdio.h>
#include		<errno.h>
#include		<unistd.h>
#include		"client.h"

int			do_connect(t_client *client)
{
  if (connect(client->sock_info.sock,
	      (const struct sockaddr *)&client->sock_info.s_in,
	      sizeof(client->sock_info.s_in)))
    {
      perror(ERR_CONNECT);
      close(client->sock_info.sock);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}
