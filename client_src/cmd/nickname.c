/*
** nickname.c for nickname in /home/naina_r//my_irc/client_src/cmd
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Fri Apr 26 14:57:17 2013 richard naina
** Last update Sun Apr 28 20:13:37 2013 richard naina
*/

#include		<string.h>
#include		"my_irc.h"
#include		"client.h"

int			nickname(t_client *client, char **tab, const char *name)
{
  if (tablen(tab) != 2)
    return (R_FAILURE);
  client->name = tab[1];
  my_send(client->sock_info.sock, name, strlen(name));
  return (R_SUCCESS);
}
