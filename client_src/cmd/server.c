/*
** server.c for server in /home/naina_r//my_irc/client_src/cmd
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Fri Apr 26 13:33:37 2013 richard naina
** Last update Sun Apr 28 20:07:53 2013 richard naina
*/

#include		<stdlib.h>
#include		<string.h>
#include		<stdio.h>
#include		"client.h"
#include		"my_irc.h"

static int		my_isdigit(char *str, char *comp)
{
  int			i;
  unsigned int		j;

  i = 0;
  while (str[i] != '\0')
    {
      j = 0;
      while (comp[j] != '\0' && comp[j] != str[i])
	++j;
      if (j > strlen(comp))
	return (R_FAILURE);
      ++i;
    }
  return (R_SUCCESS);
}

int			server(t_client *client, char **tab)
{
  if (tablen(tab) != 3)
    return (R_FAILURE);
  if (my_isdigit(tab[1], "0123456789.") == R_FAILURE)
    return (R_FAILURE);
  if (my_isdigit(tab[2], "0123456789") == R_FAILURE)
    return (R_FAILURE);
  if (set_connection(client, tab) == EXIT_FAILURE)
    return (R_FAILURE);
  return (R_SUCCESS);
}
