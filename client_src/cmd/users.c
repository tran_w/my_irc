/*
** users.c for users in /home/naina_r//my_irc/client_src/cmd
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Fri Apr 26 15:29:56 2013 richard naina
** Last update Sun Apr 28 20:14:12 2013 richard naina
*/

#include		<string.h>
#include		"client.h"
#include		"my_irc.h"

int			users(t_client *client, char **tab, const char *name)
{
  if (tablen(tab) != 1)
    return (R_FAILURE);
  my_send(client->sock_info.sock, name, strlen(name));
  return (R_SUCCESS);
}
