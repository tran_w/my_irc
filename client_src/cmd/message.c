/*
** message.c for message in /home/naina_r//my_irc/client_src/cmd
** 
** Made by richard naina
** Login   <naina_r@epitech.net>
** 
** Started on  Fri Apr 26 15:32:20 2013 richard naina
** Last update Sun Apr 28 20:16:07 2013 richard naina
*/

#include			<string.h>
#include			"my_irc.h"
#include			"client.h"

int				message(t_client *client, char **tab, const char *name)
{
  if (tablen(tab) < 3)
    return (R_FAILURE);
  my_send(client->sock_info.sock, name, strlen(name));
  return (R_SUCCESS);
}
