/*
** my_recv.c for  in /home/tran_w/Projects/my_ftp/server
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr  8 21:35:49 2013 tran_w
** Last update Mon Jun 17 22:35:25 2013 tran_w
*/

#include	<unistd.h>
#include	<errno.h>
#include	<stdio.h>
#include	"client.h"

int		my_recv(const int socket,
			char *buffer,
			const size_t len)
{
  int		byte;

  if ((byte = read(socket, buffer, len - 1)) == R_FAILURE)
    {
      perror(ERR_RECV);
      return (R_FAILURE);
    }
  buffer[byte] = '\0';
  return (byte);
}
