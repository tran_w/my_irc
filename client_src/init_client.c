/*
** init.c for  in /home/tran_w/Projects/my_irc/client_src
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr 22 16:43:15 2013 tran_w
** Last update Mon Apr 22 18:45:24 2013 tran_w
*/

#include		<stdlib.h>
#include		<stdio.h>
#include		<errno.h>
#include		"my_irc.h"
#include		"client.h"

static int     		init_sock(t_client *client)
{
  if ((client->sock_info.sock = socket(AF_INET,
				       SOCK_STREAM,
				       client->sock_info.pe->p_proto))
      == R_FAILURE)
    {
      perror(ERR_SOCKET);
      return (R_FAILURE);
    }
  client->sock_info.s_in.sin_family = AF_INET;
  return (R_SUCCESS);
}

static int     		init_protocol(t_client *client)
{
  if ((client->sock_info.pe = getprotobyname(PROTOCOL)) == NULL)
    {
      perror(ERR_PROTOCOL);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

int			init_client(t_client *client)
{
  if (init_protocol(client) == R_FAILURE)
    return (R_FAILURE);
  if (init_sock(client) == R_FAILURE)
    return (R_FAILURE);
  return (R_SUCCESS);
}
