/*
** my_send.c for  in /home/tran_w/Projects/my_irc/client_src
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Tue Apr 23 11:47:27 2013 tran_w
** Last update Sat Apr 27 17:40:48 2013 tran_w
*/

#include	<unistd.h>
#include	<errno.h>
#include	<stdio.h>
#include	"client.h"

int		my_send(const int socket,
			const char *buffer,
			const size_t len)
{
  int		byte;

  if ((byte = write(socket, buffer, len)) == R_FAILURE)
    {
      perror(ERR_SEND);
      return (R_FAILURE);
    }
  return (byte);
}
