/*
** main.c for  in /home/tran_w/Projects/my_irc/client_src
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr 22 15:06:10 2013 tran_w
** Last update Sun Apr 28 20:00:10 2013 richard naina
*/

#include		<stdlib.h>
#include		<unistd.h>
#include		<string.h>
#include		"client.h"

int			main(int ac, char **av)
{
  t_client		client;

  client.name = NULL;
  client.sock_info.sock = -1;
  usage();
  if (ac == 3)
    {
      if (set_connection(&client, av) == EXIT_FAILURE)
	return (EXIT_FAILURE);
    }
  loop(&client);
  if (client.sock_info.sock != -1)
    close(client.sock_info.sock);
  return (EXIT_SUCCESS);
}
