/*
** loop.c for  in /home/tran_w/Projects/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Tue Apr 23 11:28:17 2013 tran_w
** Last update Sun Apr 28 20:45:39 2013 tran_w
*/

#include	<unistd.h>
#include	<stdlib.h>
#include	<string.h>
#include	<stdio.h>
#include	"client.h"

static int	read_server(const int sock, char *buffer)
{
  if ((my_recv(sock, buffer, BUFFER_SIZE)) == 0 ||
    !strcmp(DISCO_MSG, buffer))
      {
	puts(KICKED_MSG);
	return (R_SUCCESS);
      }
  return (R_FAILURE);
}

static int	read_input(char *buffer)
{
  char		*pos_eol;

  if (fgets(buffer, BUFFER_SIZE, stdin))
    {
      if ((pos_eol = strchr(buffer, EOL)))
	buffer[pos_eol - buffer] = 0;
      return (R_SUCCESS);
    }
  return (R_FAILURE);
}

static int	do_select(t_client *client)
{
  int		max_fd;

  FD_ZERO(&client->rfds);
  FD_ZERO(&client->wfds);
  FD_SET(STDIN_FILENO, &client->rfds);
  FD_SET(client->sock_info.sock, &client->rfds);
  FD_SET(client->sock_info.sock, &client->wfds);
  if (client->sock_info.sock == -1)
    max_fd = STDERR_FILENO;
  else
    max_fd = client->sock_info.sock;
  if (select(max_fd + 1,
	     &client->rfds, &client->wfds, NULL, NULL) == R_FAILURE)
    {
      perror(ERR_SELECT);
      return (R_FAILURE);
    }
  return (R_SUCCESS);
}

int		loop(t_client *client)
{
  char		buffer[BUFFER_SIZE];

  while (IS_TRUE)
    {
      if (do_select(client) == R_FAILURE)
	return (R_FAILURE);
      if (FD_ISSET(STDIN_FILENO, &client->rfds))
	{
	  read_input(buffer);
	  respond(client, buffer);
	}
      else if (FD_ISSET(client->sock_info.sock, &client->rfds))
	{
	  if (read_server(client->sock_info.sock, buffer) == R_SUCCESS)
	    return (R_SUCCESS);
	  puts(buffer);
	}
    }
  return (R_SUCCESS);
}
