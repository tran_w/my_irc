/*
** set_addr.c for  in /home/tran_w/Projects/my_irc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr 22 18:24:01 2013 tran_w
** Last update Sun Apr 28 19:47:08 2013 tran_w
*/

#include		<stdlib.h>
#include		<netinet/in.h>
#include		"client.h"

int			set_connection(t_client *client, char **av)
{
  set_addr(client, av[1]);
  set_port(client, atoi(av[2]));
  if (init_client(client) == R_FAILURE)
    return (R_FAILURE);
  if (do_connect(client) == R_FAILURE)
    return (R_FAILURE);
  return (R_SUCCESS);
}

void			set_addr(t_client *client, const char *address)
{
  client->sock_info.s_in.sin_addr.s_addr = inet_addr(address);
}

void			set_port(t_client *client, const int port)
{
  client->sock_info.s_in.sin_port = htons(port);
}
