/*
** server.h for  in /home/tran_w/Projects/my_irc/inc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr 22 15:04:03 2013 tran_w
** Last update Sun Apr 28 22:01:16 2013 tran_w
*/

#ifndef			SERVER_H__
# define		SERVER_H__

# include		"simple_list.h"
# include		"my_irc.h"

# define		NB_SERV_CMDS	(8)

# define		ERR_ACCEPT	"accept()"
# define		ERR_BIND	"bind()"
# define		ERR_LISTEN	"listen()"
# define		ERR_LOOP	"Something went wrong."
# define		ERR_IP		"Error: Can't catch client ip"
# define		ERR_NAME	"Error: Can't catch client name"

# define		SERVER_USAGE	"./server <port>"
# define		SERVER_NAME	"[SERVER] :"
# define		GET_CONNECTED	"the following ip is connected \
to server."

# define		MAX_CLIENTS	(42)
# define		NAME_SIZE	(16)
# define		IP_SIZE		(256)
# define		BUFFER_SIZE	(4096)
# define		DEFAULT_NAME	"Anonymous"

typedef struct		s_channel
{
  char			*name;
  t_list		clients;
}			t_channel;

typedef struct		s_client
{
  char			*name;
  char			*ip;
  int			sock;
}			t_client;

typedef struct		s_server
{
  t_list		channels;
  t_list		clients;
  t_sock_info		s_i;
  fd_set		rfds;
  fd_set		wfds;
  int			port;
  int			fd_max;
}			t_server;

/* usage.c */
void			usage(void);

/* loop.c */
int			loop(t_server *server);

/* init_server.c */
int			init_server(t_server *server);

/* add_client.c */
int			add_client(t_server *server);

/* respond.c */
int			respond(t_server *server);

/* execute_request.c */
int			execute_request(t_server *server,
					t_client *client,
					const char *buffer);

/* send_message.c */
int			send_message(t_server *server,
				     t_client *client,
				     const char *message);

char			**my_str_to_wordtab(const char *str, char *sep);

int			tablen(char **tab);

int			join_channel(t_server *server, t_client *client, const char *cmd);

int			part_channel(t_server *server, t_client *sender, const char *cmd);

int			nickname(t_server *server, t_client *sender, const char *cmd);

int			list_channels(t_server *server, t_client *sender, const char *cmd);

int			users(t_server *server, t_client *sender);

int			message(t_server *server, t_client *sender, const char *cmd);

int			create_channel(t_server *server, const char *name);

void			free_tab(char **tab);

int			get_channel(const t_server *server, const t_client *sender);

#endif			/* !SERVER_H__ */
