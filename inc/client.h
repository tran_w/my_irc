/*
** client.h for  in /home/tran_w/Projects/my_irc/inc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr 22 15:04:19 2013 tran_w
** Last update Sun Apr 28 21:02:26 2013 tran_w
*/

#ifndef			CLIENT_H__
# define		CLIENT_H__

# include		"my_irc.h"

# define		NB_CLIENT_CMDS	(10)

# define		BUFFER_SIZE	(1024)
# define		PROMPT		"[my_irc]$ "
# define		EOL		'\n'

# define		ERR_CONNECT	"connect()"

# define		DISCO_MSG	"DISCONNECT"
# define		KICKED_MSG	"Connection with server has been \
closed"
# define		SUCCESS_CMD_MSG	"has been successfully executed."
# define		FAILURE_CMD_MSG	"has failed to execute."

typedef struct		s_client
{
  t_sock_info		sock_info;
  char			*name;
  fd_set		wfds;
  fd_set		rfds;
}			t_client;

/* usage.c */
int			usage();

/* init_client.c */
int			init_client(t_client *client);

/* setters.c */
void			set_addr(t_client *client, const char *address);
void			set_port(t_client *client, const int port);

/* do_connect.c */
int			do_connect(t_client *client);

/* loop.c */
int			loop(t_client *client);

/* respond.c */
int			respond(t_client *client, const char *buffer);

/* my_str_to_wordtab.c */
char			**my_str_to_wordtab(const char *str, char *sep);

int			server(t_client *client, char **tab);

int			tablen(char **tab);

int			set_connection(t_client *client, char **tab);

int			nickname(t_client *client, char **tab, const char *name);

int			channel(t_client *client, char **tab, const char *name);

int			users(t_client *client, char **tab, const char *name);

int			message(t_client *client, char **tab, const char *name);

void			free_tab(char **tab);

#endif			/* !CLIENT_H__ */
