/*
** my_irc.h for  in /home/tran_w/Projects/my_irc/inc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Mon Apr 22 14:52:26 2013 tran_w
** Last update Sun Apr 28 21:02:31 2013 tran_w
*/

#ifndef			MY_IRC_H__
# define		MY_IRC_H__

# include		<netdb.h>
# include		<arpa/inet.h>
# include		<sys/types.h>
# include		<sys/socket.h>

# define		R_FAILURE	(-1)
# define		R_SUCCESS	(0)
# define		IS_TRUE		(42)

# define		ERR_CLOSE	"close()"
# define		ERR_MALLOC	"malloc()"
# define		ERR_SOCKET	"socket()"
# define		ERR_PROTOCOL	"getprotobyname()"
# define		ERR_RECV	"read()"
# define		ERR_SEND	"write()"
# define		ERR_SELECT	"select()"

# define		GOODBYE		"Good bye!"
# define		NOT_IMPLEMENTED	"This command has not been \
implemented yet."

# define		PROTOCOL	"TCP"

typedef struct		s_sock_info
{
  struct protoent	*pe;
  struct sockaddr_in	s_in;
  int			sock;
}			t_sock_info;

typedef struct		s_commands
{
  char			*name;
  int			(*ptr_func)();
}			t_commands;

/* my_send.c */
int			my_send(const int socket,
				const char *buffer,
				const size_t len);
/* my_recv.c */
int			my_recv(const int socket,
				char *buffer,
				const size_t len);

#endif			/* !MY_IRC_H__ */
