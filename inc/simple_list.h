/*
** simple_list.h for  in /home/tran_w/Projects/my_irc/inc
** 
** Made by tran_w
** Login   <tran_w@epitech.net>
** 
** Started on  Wed Apr 24 17:03:50 2013 tran_w
** Last update Fri Apr 26 18:01:02 2013 duy-laurent tran
*/

#ifndef			SIMPLE_LIST_H_
# define		SIMPLE_LIST_H_

/*
** Types
*/

typedef enum		e_bool
{
    FALSE,
    TRUE
}			t_bool;

typedef struct		s_node
{
  void			*elem;
  struct s_node		*next;
}			t_node;

typedef t_node		*t_list;

/*
** Functions
*/

/* Informations */

unsigned int	       	list_get_size(t_list list);
t_bool			list_is_empty(t_list list);
/* void *			list_dump(t_list list); */

/* Modification */

t_bool			list_add_elem_at_front(t_list *front_ptr, void *elem);
t_bool			list_add_elem_at_back(t_list *front_ptr, void *elem);
t_bool			list_add_elem_at_position(t_list *front_ptr,
						  void *elem,
						  unsigned int position);

t_bool			list_del_elem_at_front(t_list *front_ptr);
t_bool			list_del_elem_at_back(t_list *front_ptr);
t_bool			list_del_elem_at_position(t_list *front_ptr,
						  unsigned int position);

/* Value Access */

void			*list_get_elem_at_front(t_list list);
void			*list_get_elem_at_back(t_list list);
void			*list_get_elem_at_position(t_list list,
						  unsigned int position);

/* Node Access */

t_node			*list_get_first_node_with_sock(t_list list, void *value);


#endif			/* !SIMPLE_LIST_H_ */
