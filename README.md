# my_irc
The aim of this project is to realise a IRC client / server.

## Usage
* /help : show usage.
* /server _host_[ :_port_] : connects to a server.
* /nick _nickname_ : deﬁnes the nickname of the user in the channel.
* /list [string] : list the channels available on the server. Displays only the channels containing the string "string" if it is speciﬁed.
* /join _channel_ : joins a channel on server.
* /part _channel_ : leave the channel.
* /users : display the users connected to the channel (display the nicknames of course).
* _message_ : sends a message to all users connected to the channel.
* /msg _nickname_ _message_ : sends a message to a speciﬁc user.
* /send_file _nickname_ _file_ : sends a ﬁle to a user.
* /accept_file _nickname_ : accepts the reception of a ﬁle from a user from the
channel.

## About
This project was realized in C.