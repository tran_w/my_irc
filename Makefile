##
## Makefile for  in /home/tran_w/Projects/my_irc
## 
## Made by tran_w
## Login   <tran_w@epitech.net>
## 
## Started on  Mon Jun 17 22:36:10 2013 tran_w
## Last update Mon Jun 17 22:36:13 2013 tran_w
##

SERVER_NAME	=	server
CLIENT_NAME	=	client

SERVER_DIR	=	./server_src
CLIENT_DIR	=	./client_src
INCLUDE_DIR	=	./inc

SERVER_SRCS	=	$(SERVER_DIR)/main.c \
			$(SERVER_DIR)/init_server.c \
			$(SERVER_DIR)/loop.c \
			$(SERVER_DIR)/usage.c \
			$(SERVER_DIR)/list/simple_list.c \
			$(SERVER_DIR)/list/add_simple_list.c \
			$(SERVER_DIR)/list/get_simple_list.c \
			$(SERVER_DIR)/list/del_simple_list.c \
			$(SERVER_DIR)/add_client.c \
			$(SERVER_DIR)/my_recv.c \
			$(SERVER_DIR)/my_send.c \
			$(SERVER_DIR)/respond.c \
			$(SERVER_DIR)/execute_request.c \
			$(SERVER_DIR)/send_message.c \
			$(SERVER_DIR)/create_channel.c \
			$(SERVER_DIR)/cmd/my_str_to_wordtab.c \
			$(SERVER_DIR)/cmd/tablen.c \
			$(SERVER_DIR)/cmd/channel.c \
			$(SERVER_DIR)/cmd/users.c \
			$(SERVER_DIR)/cmd/message.c


CLIENT_SRCS	=	$(CLIENT_DIR)/main.c \
			$(CLIENT_DIR)/init_client.c \
			$(CLIENT_DIR)/setters.c \
			$(CLIENT_DIR)/do_connect.c \
			$(CLIENT_DIR)/loop.c \
			$(CLIENT_DIR)/my_recv.c \
			$(CLIENT_DIR)/my_send.c \
			$(CLIENT_DIR)/respond.c	\
			$(CLIENT_DIR)/my_str_to_wordtab.c \
			$(CLIENT_DIR)/cmd/server.c \
			$(CLIENT_DIR)/cmd/nickname.c \
			$(CLIENT_DIR)/cmd/channel.c \
			$(CLIENT_DIR)/cmd/users.c \
			$(CLIENT_DIR)/cmd/message.c \
			$(CLIENT_DIR)/tablen.c \
			$(CLIENT_DIR)/usage.c

SERVER_OBJS	=	$(SERVER_SRCS:.c=.o)
CLIENT_OBJS	=	$(CLIENT_SRCS:.c=.o)

CFLAGS		+=	-I$(INCLUDE_DIR)

CC		=	gcc

all		:	$(SERVER_NAME) $(CLIENT_NAME)

s		:	$(SERVER_NAME)

c		:	$(CLIENT_NAME)

$(SERVER_NAME)	:	$(SERVER_OBJS)
			$(CC) -o $(SERVER_NAME) $(SERVER_OBJS)

$(CLIENT_NAME)	:	$(CLIENT_OBJS)
			$(CC) -o $(CLIENT_NAME) $(CLIENT_OBJS)

clean		:
			$(RM) $(SERVER_OBJS) $(CLIENT_OBJS)

fclean		:	clean
			$(RM) $(SERVER_NAME) $(CLIENT_NAME)

flush		:	fclean
			$(RM) *~
			$(RM) \#*#
			$(RM) $(SERVER_DIR)/*~
			$(RM) $(CLIENT_DIR)/*~
			$(RM) $(INCLUDE_DIR)/*~
			$(RM) $(SERVER_DIR)/\#*#
			$(RM) $(INCLUDE_DIR)/\#*#
			$(RM) $(CLIENT_DIR)/\#*#

re		:	fclean all

.PHONY		:	clean fclean re all s c flush
